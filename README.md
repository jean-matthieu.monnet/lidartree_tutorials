# IMPORTANT NOTICE

* `lidaRtRee_tutorials` are now integrated as [vignettes](https://forgemia.inra.fr/lidar/lidaRtRee/-/tree/main/vignettes) of the R package [`lidaRtRee`](https://forgemia.inra.fr/lidar/lidaRtRee).
* Documentation and tutorials are on [https://lidar.pages.mia.inra.fr/lidaRtRee/](https://lidar.pages.mia.inra.fr/lidaRtRee/)
